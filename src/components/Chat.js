import React, {Component} from 'react';

import{
  AppRegistry,
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  NativeModules
} from 'react-native';

export default class Chat extends Component {
      constructor(){
         super();
         this.state = {
           name: 'Brad',
           showName: true,
           surname: 'johns'
         };
        var PointziReact = NativeModules.PointziReact;
        var cuid = 'ethan_rn';
        PointziReact.tagCuid(cuid);
        PointziReact.tagString('ethan','qa_first_name');
        PointziReact.tagNumeric(12, 'qa_number');
        PointziReact.tagDatetime("2018-05-15T16:12:22.123Z", 'qa_datetime');
      }
     render(){
      let name = this.state.showName ? this.state.name : 'No name';
       return(
         <View style={style.myView}  nativeID="Chatting View">
           <TouchableHighlight nativeID="brad btn"
               onPress={() => { this.saveSign() } } >
             <Text style={style.myText}>
               {this.state.name}
             </Text>
          </TouchableHighlight>
          <TouchableHighlight nativeID="reset btn"
               onPress={() => { this.resetSign() } } >
             <Text>
                {this.state.surname}
             </Text>
          </TouchableHighlight>
         </View>
       )
     }

     saveSign() {

     }

     resetSign() {
          
     }
}

const style = StyleSheet.create({
   myView: {
       backgroundColor: 'grey'
   },
   myText: {
     color: 'red'
   }
});

AppRegistry.registerComponent('Chat', () => Chat);
